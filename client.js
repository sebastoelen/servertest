const WebSocket = require('ws');

var connection = new WebSocket('ws://localhost:9030');

connection.onopen = function () {
    console.log("Connection opened");

};

// Log errors
connection.onerror = function (error) {
    console.error('WebSocket Error ' + error);
};

// Log messages from the server
connection.onmessage = function (e) {

};